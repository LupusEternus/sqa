#include <cstring>

class CStringWrapper
{
    char* data;
public:
    CStringWrapper(const char* src)
    {
        data = strdup(src);
    }
    ~CStringWrapper()
    {
        delete[] data;
    }
};


int main()
{
    CStringWrapper obj{"My very own string"};

    return 0;
}
